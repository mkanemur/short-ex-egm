EGamma exercise
---------------
This exercise provdes an introduction to CMS electron and photon objects.

We will use https://swan.cern.ch

When selecting the environment, please use software stack "105a", Platform "CentOS7 (gcc11)", maximum cores and maximum memory.

Click on the top right `+` button to create a new project and name it "CMSDAS_CERN_2024". Open a new terminal tab by clicking on `>_` and then run the commands below:

>`cd SWAN_projects/CMSDAS_CERN_2024/`

>`git clone https://gitlab.cern.ch/cmsdas-cern-2024/short-ex-egm.git`

Now, go back to SWAN tab, and open the "short-ex-egm" folder.

We will start with `exercise-1.ipynb` and then move to `exercise-2.ipynb`.

Some references:
 - [CMSDAS CERN 2024](https://indico.cern.ch/event/1388937/)
 - [CMSDAS CERN 2023](https://indico.cern.ch/event/1257234/)
 - [CMSDAS LPC 2023](https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideCMSDataAnalysisSchoolLPC2023EGammaShortExercise)
 - [CMSDAS 2021](https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideCMSDataAnalysisSchoolLPC2021EGammaExercise)
 - [LPC HATS 2020](https://twiki.cern.ch/twiki/bin/view/CMS/EGammaHATSatLPC2020)
 - [CMSDAS 2020](https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideCMSDataAnalysisSchoolLPC2020EGammaExercise)
